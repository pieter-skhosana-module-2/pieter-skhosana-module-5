import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'display.dart';
import 'home.dart';
import 'package:flutter/material.dart';

class ParkingBar extends StatefulWidget {
  const ParkingBar({Key? key}) : super(key: key);

  @override
  State<ParkingBar> createState() => _ParkingBarState();
}

class _ParkingBarState extends State<ParkingBar> {
  //declaring the variable to hold user inputs
  TextEditingController name = TextEditingController();
  TextEditingController carMake = TextEditingController();
  TextEditingController carDetails = TextEditingController();
  TextEditingController parkDate = TextEditingController();
  TextEditingController parkingDuration = TextEditingController();

  //method to process the user inputs to the firebase db
  Future<void> requestBooking() {
    final nameInput = name.text;
    final carMakeInput = carMake.text;
    final details = carDetails.text;
    final dateInput = parkDate.text;
    final durationInput = parkingDuration.text;

    final ref = FirebaseFirestore.instance.collection("bookings").doc();

    return ref
        .set({
          "Name": nameInput,
          "Car Make": carMakeInput,
          "Car Details": details,
          "Date": dateInput,
          "Parking Duration": durationInput,
          "doc_id": ref.id,
        })
        .then((value) => {
              showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  title: const Text("Booking Request"),
                  content: const Text("Your booking was submitted succesfully"),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text("Close"))
                  ],
                ),
              ),
              name.text = "",
              carMake.text = "",
              carDetails.text = "",
              parkDate.text = "",
              parkingDuration.text = "",
            })
        .catchError((onError) => log(onError));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Book For Parking",
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: TextField(
                controller: name,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20)),
                    ),
                    hintText: "Enter Your name")),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: TextField(
                controller: carMake,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20)),
                    ),
                    hintText: "Car Make")),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: TextField(
                controller: carDetails,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20)),
                    ),
                    hintText: "CarDetails (e.g. SUV, Red)")),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: TextField(
                controller: parkDate,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20)),
                    ),
                    hintText: "Date (DD/MM/YYYY)")),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: TextField(
                controller: parkingDuration,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20)),
                    ),
                    hintText: "Parking duration (e.g. 30 minutes)")),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(50)))),
                  onPressed: () {
                    requestBooking();
                  },
                  child: const Text("Book")),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(50)))),
                  onPressed: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const ShowBooking()));
                  },
                  child: const Text("Show Booking"))
            ],
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.logout),
          onPressed: () {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => const HomeScreen()));
          }),
    );
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebaseapp/parking.dart';
import 'package:flutter/material.dart';

class ShowBooking extends StatefulWidget {
  const ShowBooking({Key? key}) : super(key: key);

  @override
  State<ShowBooking> createState() => _ShowBookingState();
}

class _ShowBookingState extends State<ShowBooking> {
  //collect data from firebase
  final Stream<QuerySnapshot> parkingBookings =
      FirebaseFirestore.instance.collection("bookings").snapshots();

  @override
  Widget build(BuildContext context) {
    //method to delete a booking
    void deleteRecord(docID) {
      FirebaseFirestore.instance
          .collection("bookings")
          .doc(docID)
          .delete()
          .then((value) => null);
    }

    //text inputs to handle the reccord editing
    TextEditingController nameUpdate = TextEditingController();
    TextEditingController carMakeUpdate = TextEditingController();
    TextEditingController carDetailsUpdate = TextEditingController();
    TextEditingController parkDateUpdate = TextEditingController();
    TextEditingController parkingDurationUpdate = TextEditingController();
    //method to update a booking record
    void updateRecord(record) {
      //get the record from firebase
      var bookingRecord = FirebaseFirestore.instance.collection("bookings");
      //populate the input fileds with the firebase data
      nameUpdate.text = record["Name"];
      carMakeUpdate.text = record["Car Make"];
      carDetailsUpdate.text = record["Car Details"];
      parkDateUpdate.text = record["Date"];
      parkingDurationUpdate.text = record["Parking Duration"];

      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: const Text("Edit Booking Record"),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextField(
                      controller: nameUpdate,
                    ),
                    TextField(
                      controller: carMakeUpdate,
                    ),
                    TextField(
                      controller: carDetailsUpdate,
                    ),
                    TextField(
                      controller: parkDateUpdate,
                    ),
                    TextField(
                      controller: parkingDurationUpdate,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: TextButton(
                        onPressed: () {
                          bookingRecord.doc(record["doc_id"]).update({
                            "Name": nameUpdate.text,
                            "Car Make": carMakeUpdate.text,
                            "Car Details": carDetailsUpdate.text,
                            "Date": parkDateUpdate.text,
                            "Parking Duration": parkingDurationUpdate.text,
                          });
                          Navigator.pop(context);
                        },
                        child: const Text(
                          "Update",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        style: TextButton.styleFrom(
                          backgroundColor: Colors.blueGrey,
                        ),
                      ),
                    )
                  ],
                ),
              ));
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text("Bookings Log"),
        centerTitle: true,
      ),
      body: SizedBox(
        child: StreamBuilder(
          stream: parkingBookings,
          builder: (BuildContext context,
              AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
            //check errors
            if (snapshot.hasError) {
              return const Text("An error occured, Please try again");
            }
            //if data is not ready,show loading circular annimation
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.deepPurpleAccent,
                ),
              );
            }

            if (snapshot.hasData) {
              return Row(
                children: [
                  Expanded(
                      child: SizedBox(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: ListView(
                      children: snapshot.data!.docs
                          .map((DocumentSnapshot documentSnapShot) {
                        Map<String, dynamic> data =
                            documentSnapShot.data()! as Map<String, dynamic>;

                        return Column(children: [
                          Card(
                            child: Column(
                              children: [
                                ListTile(
                                  tileColor: Colors.deepPurpleAccent,
                                  textColor: Colors.white,
                                  contentPadding: const EdgeInsets.all(20),
                                  leading: Text(data["Name"]),
                                  title: Text(data["Car Make"]),
                                  subtitle: Text(data["Date"]),
                                  trailing: Text(data["Parking Duration"]),
                                ),
                                ButtonTheme(
                                    child: ButtonBar(
                                  children: [
                                    ElevatedButton(
                                        onPressed: () {
                                          updateRecord(data);
                                        },
                                        child: const Icon(
                                          Icons.edit,
                                        )),
                                    ElevatedButton(
                                        onPressed: () {
                                          deleteRecord(data["doc_id"]);
                                        },
                                        child: const Text("Delete")),
                                  ],
                                ))
                              ],
                            ),
                          )
                        ]);
                      }).toList(),
                    ),
                  )),
                ],
              );
            } else {
              return (const Text("Your Booking log is empty"));
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.done),
          onPressed: () {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => const ParkingBar()));
          }),
    );
  }
}

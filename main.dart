import 'package:flutter/material.dart';
import 'splashscreen.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyDr34nhhSTMW_S7hQHZwF_wItOY9UFzoxc",
          authDomain: "fir-app-bdd02.firebaseapp.com",
          projectId: "fir-app-bdd02",
          storageBucket: "fir-app-bdd02.appspot.com",
          messagingSenderId: "136603480051",
          appId: "1:136603480051:web:da5b002545d3fb51326c86"));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //remove the debug banner
      debugShowCheckedModeBanner: false,
      //Display the spalsh screen before the home page
      home: const SplashScreen(),
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.blueGrey)
            .copyWith(secondary: Colors.deepPurpleAccent),
        textTheme: const TextTheme(
            bodyText2: TextStyle(color: Color.fromARGB(255, 48, 24, 134))),
      ),
    );
  }
}

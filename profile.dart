import 'package:firebaseapp/register.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class UserProfile extends StatefulWidget {
  const UserProfile({Key? key}) : super(key: key);

  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  //collect data from firebase
  final Stream<QuerySnapshot> userDetails =
      FirebaseFirestore.instance.collection("users").snapshots();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("User Account"),
        centerTitle: true,
      ),
      body: SizedBox(
        child: StreamBuilder(
          stream: userDetails,
          builder: (BuildContext context,
              AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
            //check errors
            if (snapshot.hasError) {
              return const Text("An error occured, Please try again");
            }
            //if data is not ready,show loading circular annimation
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.deepPurpleAccent,
                ),
              );
            }
            if (snapshot.hasData) {
              return Row(
                children: [
                  Expanded(
                      child: SizedBox(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: ListView(
                      children: snapshot.data!.docs
                          .map((DocumentSnapshot documentSnapShot) {
                        Map<String, dynamic> data =
                            documentSnapShot.data()! as Map<String, dynamic>;

                        return Column(children: [
                          Card(
                            child: Column(
                              children: [
                                ListTile(
                                  tileColor: Colors.deepPurpleAccent,
                                  textColor: Colors.white,
                                  contentPadding: const EdgeInsets.all(20),
                                  leading: Text(data["Name"]),
                                  title: Text(data["Username"]),
                                  subtitle: Text(data["Phone Number"]),
                                  trailing: Text(data["Password"]),
                                ),
                              ],
                            ),
                          ),
                        ]);
                      }).toList(),
                    ),
                  )),
                ],
              );
            } else {
              return (const Text("No information retrieved"));
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.done),
          onPressed: () {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => const Register()));
          }),
    );
  }
}

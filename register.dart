import 'dart:developer';
import 'package:firebaseapp/home.dart';
import 'package:firebaseapp/profile.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  //declaring the variable to hold user details
  TextEditingController fullName = TextEditingController();
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController gender = TextEditingController();
  TextEditingController phone = TextEditingController();

  //method to process the user inputs to the firebase db
  Future<void> createProfile() {
    final userFullname = fullName.text;
    final userName = username.text;
    final userPassword = password.text;
    final userGender = gender.text;
    final userPhone = phone.text;

    final ref = FirebaseFirestore.instance.collection("users").doc();

    return ref
        .set({
          "Name": userFullname,
          "Username": userName,
          "Password": userPassword,
          "Gender": userGender,
          "Phone Number": userPhone,
          "doc_id": ref.id,
        })
        .then((value) => {
              showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  title: const Text("User Profile"),
                  content: const Text("Your profile was added succesfully"),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text("Close"))
                  ],
                ),
              ),
              fullName.text = "",
              username.text = "",
              password.text = "",
              gender.text = "",
              phone.text = "",
            })
        .catchError((onError) => log(onError));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Create/Edit Profile"),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(10),
            child: TextField(
                controller: fullName,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20)),
                    ),
                    hintText: "Enter Your Full Name")),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: TextField(
                controller: username,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20)),
                    ),
                    hintText: "Username")),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: TextField(
                controller: password,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20)),
                    ),
                    hintText: "Password")),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: TextField(
                controller: gender,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20)),
                    ),
                    hintText: "Gender")),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: TextField(
                controller: phone,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20)),
                    ),
                    hintText: "Phone Number")),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              //const Padding(padding: EdgeInsets.all(10)),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(50)))),
                  onPressed: () {
                    createProfile();
                  },
                  child: const Text("Confirm")),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(50)))),
                  onPressed: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const HomeScreen()));
                  },
                  child: const Text("Login")),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(50)))),
                  onPressed: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const UserProfile()));
                  },
                  child: const Text("View Profile")),
            ],
          )
        ],
      ),
    );
  }
}
